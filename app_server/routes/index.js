var express = require('express');
var router = express.Router();
//var ctrlMain = require('../controllers/main');
var cLocations = require('../controllers/locations');
var cOthers = require('../controllers/others');

/* GET home page. */
//router.get('/', ctrlMain.index);

// Locations Pages
router.get('/', cLocations.homelist);
router.get('/location', cLocations.locationInfo);
router.get('/location/review/new', cLocations.addReview);

// Other Pages
router.get('/about', cOthers.about);

module.exports = router;



